export class Logger {
  fields: Record<string, any>;

  constructor() {
    this.fields = {};
  }

  withFields(fields: Record<string, any>): Logger {
    let l = new Logger();
    l.fields = { ...this.fields, ...fields };
    return l;
  }

  private logfields(level: string, ...data: any) {
    let f = {
      time: (new Date()).toUTCString(),
      level,
      ...this.fields
    };
    return !!data.length
      ? { ...f, message: data }
      : f;
  }

  trace(...data: any) { console.trace(this.logfields("warn", ...data)) }
  debug(...data: any) { console.debug(this.logfields("warn", ...data)) }
  info(...data: any) { console.info(this.logfields("info", ...data)) }
  warn(...data: any) { console.warn(this.logfields("warn", ...data)) }
  error(...data: any) { console.error(this.logfields("error", ...data)) }
}
